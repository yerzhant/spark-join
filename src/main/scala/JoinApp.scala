import org.apache.spark.sql.SparkSession

object JoinApp {
  def main(args: Array[String]): Unit = {
    val dataDir = "data/join/"
    val clientTypesSource = dataDir + "client-types.csv"
    val citiesSource = dataDir + "cities.csv"
    val txnsSource = dataDir + "generated-txns.csv"

    val spark = SparkSession.builder().appName("Joiner").getOrCreate()

    val clientTypesDF = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(clientTypesSource)

    val citiesDF = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(citiesSource)

    val txnsDF = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(txnsSource)

    val theDslWay = new TheDslWay(spark, dataDir, clientTypesDF, citiesDF, txnsDF)
    theDslWay.calc()

    val theSqlWay = new TheSqlWay(spark, dataDir, clientTypesDF, citiesDF, txnsDF)
    theSqlWay.calc()

    spark.stop()
  }
}
