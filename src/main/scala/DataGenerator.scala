import java.io.{BufferedWriter, File, FileWriter}
import java.time.{LocalDateTime, ZoneOffset}

import scala.util.Random

object DataGenerator extends App {
  val minDate = LocalDateTime.of(2017, 8, 1, 0, 0, 0)
  val maxDate = LocalDateTime.now()
  val txnCount = 1000 * 1000
  val clientTypes = 2
  val cities = 5

  val file = new File("test-data/generated-txns.csv")
  val bw = new BufferedWriter(new FileWriter(file))

  try {
    bw.write("id,date,client_type_id,city_id,billed_amount,data\n")

    var id = 1
    while (id <= txnCount) {
      val date = generateDate()
      val clientTypeId = Random.nextInt(clientTypes) + 1
      val cityId = Random.nextInt(cities) + 1
      val billedAmount = Random.nextInt(1500) + "." + Random.nextInt(100) // Here maybe 0.0! Come on! It's just a test.
      val details = "Some dummy cool data"

      bw.write(s"$id,$date,$clientTypeId,$cityId,$billedAmount,$details\n")

      id += 1
    }
  } finally {
    bw.close()
  }

  private def generateDate() = {
    val diff = maxDate.toEpochSecond(ZoneOffset.MAX) - minDate.toEpochSecond(ZoneOffset.MAX)
    val random = Random.nextInt(diff.toInt)
    minDate.plusSeconds(random)
  }
}
