import org.apache.spark.sql.functions.date_trunc
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

class TheSqlWay(spark: SparkSession, dataDir: String, clientTypesDF: DataFrame, citiesDF: DataFrame, txnsDF: DataFrame) {

  def calc(): Unit = {
    import spark.implicits._

    clientTypesDF.createOrReplaceTempView("client_types")

    citiesDF.createOrReplaceTempView("cities")

    txnsDF.select(
      $"client_type_id",
      $"city_id",
      date_trunc("day", $"date").as("date"),
      $"billed_amount"
    ).createOrReplaceTempView("txns")

    val byMonths = spark.sql(
      """select ct.name client_type, c.name city, date_trunc('month', date) date, sum(billed_amount) billed_amount
                   from txns t
                   join client_types ct on ct.id = t.client_type_id
                   join cities c on c.id = t.city_id
                  where year(date) = 2018
                  group by ct.name, c.name, date_trunc('month', date)""")

    val fullYear2018 = spark.sql(
      """select ct.name client_type, c.name city, sum(billed_amount) billed_amount
                   from txns t
                   join client_types ct on ct.id = t.client_type_id
                   join cities c on c.id = t.city_id
                  where year(date) = 2018
                  group by ct.name, c.name""")

    byMonths.write.mode(SaveMode.Overwrite).csv(dataDir + "sql-by-months")
    fullYear2018.write.mode(SaveMode.Overwrite).csv(dataDir + "sql-full-year-2018")
  }
}
