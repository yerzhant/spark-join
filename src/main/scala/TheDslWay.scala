import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions.{date_trunc, sum, year}

class TheDslWay(spark: SparkSession, dataDir: String, clientTypesDF: DataFrame, citiesDF: DataFrame, txnsDF: DataFrame) {

  def calc() {
    import spark.implicits._

    val joinedDF = txnsDF
      .join(clientTypesDF, clientTypesDF("id") === txnsDF("client_type_id"))
      .join(citiesDF, citiesDF("id") === txnsDF("city_id"))

    val truncatedAndShortenedDF = joinedDF
      .select(
        date_trunc("day", $"date").as("date"),
        $"billed_amount",
        clientTypesDF("name").as("client_type"),
        citiesDF("name").as("city")
      )

    val year2018byDayDF = truncatedAndShortenedDF
      .filter(year($"date") === 2018)
      .groupBy($"client_type", $"city", $"date")
      .agg(sum($"billed_amount").as("billed_amount")).cache()

    val byMonthsDF = year2018byDayDF
      .groupBy($"client_type", $"city", date_trunc("month", $"date"))
      .agg(sum($"billed_amount"))

    val overFull2018YearDF = year2018byDayDF
      .groupBy($"client_type", $"city", date_trunc("year", $"date"))
      .agg(sum($"billed_amount"))

    //        byMonthsDF.show()
    byMonthsDF.write.mode(SaveMode.Overwrite).csv(dataDir + "by-months")
    //        overFull2018YearDF.show()
    overFull2018YearDF.write.mode(SaveMode.Overwrite).csv(dataDir + "full-year-2018")
  }
}
